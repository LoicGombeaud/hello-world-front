# hello-world-front

## Purpose

A simplistic "Hello world" front, calling a back.

## Usage

The environment variable `BACK_URL` must be provided at runtime.

## Build

```
TAG=<tag>
docker build -t loicag/hello-world-front:$TAG
docker push loicag/hello-world-front:TAG
```
