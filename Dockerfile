FROM nginx:alpine

COPY configure_back /usr/local/bin

COPY html /usr/share/nginx/html

COPY start.sh .

CMD "./start.sh"
